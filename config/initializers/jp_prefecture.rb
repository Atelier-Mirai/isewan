custom_mapping_path = "config/initializers/prefecture_area.yml" # /path/to/mapping_data

JpPrefecture.setup do |config|
  config.mapping_data = YAML.load_file custom_mapping_path
end
