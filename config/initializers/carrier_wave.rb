if Rails.env.production?
  CarrierWave.configure do |config|
    config.fog_credentials = {
      # Amazon S3 用の設定
      :provider              => 'AWS',
      :aws_access_key_id     => ENV['S3_ACCESS_KEY'],
      :aws_secret_access_key => ENV['S3_SECRET_KEY'],
      :region                => ENV['S3_REGION'],
    }
    config.fog_directory = ENV['S3_BUCKET']
    # config.cache_storage = :fog #キャッシュの場所をS3に変更
    # config.cache_dir     = "#{Rails.root}/tmp/uploads" #for Heroku
    # config.asset_host = 'https://s3-ap-northeast-1.amazonaws.com/dummy'
  end
end
