Rails.application.routes.draw do
  mount PdfjsViewer::Rails::Engine => "/pdfjs", as: 'pdfjs'

  root 'welcome#index'
  resource  :landing,        only: [:edit, :update]
  resource  :intro,          only: [:edit, :update]
  get 'whatsnews/lists' => 'whatsnews#lists'
  resources :whatsnews,      only: [:index, :create, :update] do
    patch :sort
  end
  resources :winners,        only: [:index, :create, :edit, :update, :destroy]
  resource  :attention,      only: [:edit,  :update, :destroy]
  resources :seeds
  resources :scores

  resources :galleries,      only: [:index, :new, :create, :update, :destroy] do
    patch :sort
  end
  resources :photos,         only: [:index, :new, :create, :update, :destroy]
  resources :movies,         only: [:index, :new, :create, :update, :destroy]

  resources :external_links, only: [:create, :update, :destroy] do
    patch :sort
  end
  resources :photo_links,    only: [:create, :update, :destroy]
  resources :movie_links,    only: [:create, :update, :destroy]

  resources :events,         only: [:index, :new, :create, :update, :destroy]
  resources :s3files,        only: [:destroy]
  resources :preliminaries,  only: [:index, :new, :create, :update, :destroy]
  resources :regionals,      only: [:index, :new, :create, :update, :destroy]
  resources :viewers,        only: [:show]

  resources :pdfs

  get    'login'   => 'sessions#new'
  post   'login'   => 'sessions#create'
  match  '/logout' => 'sessions#destroy', via: [:get, :delete]

  resource :infomation,     only: [:show]

  # error
  match '*path' => 'application#error404', via: :all if Rails.env.production?
end
