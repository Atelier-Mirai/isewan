# 伊勢湾カップ実行委員会 公式Webサイト

## 参考書籍
* Ruby on Rails チュートリアル
* パーフェクト Ruby on Rails
* 実践 Ruby on Rails 4

## 環境
* Ruby on Rails 6.0.0
* Ruby 2.6.5
* Atom 1.40.1
* Bootstrap 4.3.1
