# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# This file is the source Rails uses to define your schema when running `rails
# db:schema:load`. When creating a new database, `rails db:schema:load` tends to
# be faster and is potentially less error prone than running all of your
# migrations from scratch. Old migrations may fail to apply correctly if those
# migrations use external dependencies or application code.
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2018_04_03_044141) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "attentions", id: :serial, force: :cascade do |t|
    t.string "title"
    t.text "article"
    t.string "picture"
    t.string "caption"
    t.boolean "display"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "events", id: :serial, force: :cascade do |t|
    t.integer "area"
    t.date "start_date"
    t.date "end_date"
    t.string "schedule_adjustment"
    t.string "name"
    t.string "place"
    t.string "contact"
    t.string "phone"
    t.string "url"
    t.string "type"
    t.boolean "display"
    t.string "email"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "external_links", id: :serial, force: :cascade do |t|
    t.string "text"
    t.string "url"
    t.string "type"
    t.integer "row_order"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "galleries", id: :serial, force: :cascade do |t|
    t.string "picture"
    t.string "caption"
    t.boolean "display"
    t.integer "row_order"
    t.string "type"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "intros", id: :serial, force: :cascade do |t|
    t.text "article"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "landings", id: :serial, force: :cascade do |t|
    t.text "article"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "movie_links", force: :cascade do |t|
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "photo_links", force: :cascade do |t|
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "photos", force: :cascade do |t|
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "preliminaries", force: :cascade do |t|
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "regionals", force: :cascade do |t|
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "s3files", id: :serial, force: :cascade do |t|
    t.string "title"
    t.string "file_name"
    t.string "comment"
    t.integer "event_id"
    t.integer "attention_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "scores", id: :serial, force: :cascade do |t|
    t.integer "year"
    t.string "provisional_or_decision"
    t.text "data"
    t.text "info"
    t.text "note"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "seeds", force: :cascade do |t|
    t.integer "year"
    t.text "article"
    t.text "isewan_seeds"
    t.integer "isewan_male"
    t.integer "isewan_female"
    t.text "techno_seeds"
    t.integer "techno_male"
    t.integer "techno_female"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "users", id: :serial, force: :cascade do |t|
    t.string "name"
    t.string "password_digest"
    t.string "remember_digest"
    t.boolean "admin", default: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["remember_digest"], name: "index_users_on_remember_digest"
  end

  create_table "whatsnews", id: :serial, force: :cascade do |t|
    t.string "title"
    t.text "article"
    t.boolean "display"
    t.integer "row_order"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "winners", id: :serial, force: :cascade do |t|
    t.integer "times"
    t.string "isewan_name"
    t.string "techno_name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

end
