class CreateIntros < ActiveRecord::Migration[5.0]
  def change
    create_table :intros do |t|
      t.text   :article   # 投稿記事 markdown形式

      t.timestamps
    end
  end
end
