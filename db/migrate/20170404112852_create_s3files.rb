class CreateS3files < ActiveRecord::Migration[5.0]
  def change
    create_table :s3files do |t|
      t.string  :title
      t.string  :file_name
      t.string  :comment
      t.integer :event_id
      t.integer :attention_id

      t.timestamps
    end
  end
end
