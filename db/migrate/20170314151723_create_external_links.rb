class CreateExternalLinks < ActiveRecord::Migration[5.0]
  def change
    create_table :external_links do |t|
      t.string  :text      # リンク文字列
      t.string  :url       # google photo, vimeoのURL
      t.string  :type      # photo or movie
      t.integer :row_order # 表示順

      t.timestamps
    end
  end
end
