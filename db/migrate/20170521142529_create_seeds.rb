class CreateSeeds < ActiveRecord::Migration[5.1]
  def change
    create_table :seeds do |t|
      t.integer :year # 何年の大会か
      t.text    :article # 案内文章
      t.text    :isewan_seeds  # シード選手データ(シリアライズする）
      t.integer :isewan_male
      t.integer :isewan_female
      t.text    :techno_seeds  # シード選手データ(シリアライズする）
      t.integer :techno_male
      t.integer :techno_female

      t.timestamps
    end
  end
end
