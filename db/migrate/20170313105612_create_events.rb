class CreateEvents < ActiveRecord::Migration[5.0]
  def change
    create_table :events do |t|
      t.integer :area       # 関東・九州など (都道府県コード)
      t.date    :start_date # 大会 開始日
      t.date    :end_date   # 大会 終了日
      t.string  :schedule_adjustment # 日程調整中, 5月中旬など 日程未定の場合
      t.string  :name       # 大会名
      t.string  :place      # 開催場所
      t.string  :contact    # 問い合わせ先
      t.string  :phone      # 問い合わせ先電話番号
      t.string  :url        # URL
      t.string  :type
      t.boolean :display    # 表示するならtrue
      t.string  :email

      t.timestamps
    end
  end
end
