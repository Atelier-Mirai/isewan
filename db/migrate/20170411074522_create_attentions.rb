class CreateAttentions < ActiveRecord::Migration[5.0]
  def change
    create_table :attentions do |t|
      t.string  :title     # タイトル
      t.text    :article   # 投稿記事 markdown形式
      t.string  :picture   # 写真
      t.string  :caption   # 写真のキャプション
      t.boolean :display   # 表示するならtrue

      t.timestamps
    end
  end
end
