class CreateGalleries < ActiveRecord::Migration[5.0]
  def change
    create_table :galleries do |t|
      t.string  :picture   # 写真
      t.string  :caption   # 写真や動画のキャプション
      t.boolean :display   # 表示するならtrue
      t.integer :row_order # 表示順
      t.string  :type

      t.timestamps
    end
  end
end
