class CreateWhatsnews < ActiveRecord::Migration[5.0]
  def change
    create_table :whatsnews do |t|
      t.string  :title     # タイトル
      t.text    :article   # 投稿記事 markdown形式
      t.boolean :display   # 表示するならtrue
      t.integer :row_order # 表示順

      t.timestamps
    end
  end
end
