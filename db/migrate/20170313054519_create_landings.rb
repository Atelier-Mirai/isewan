class CreateLandings < ActiveRecord::Migration[5.0]
  def change
    create_table :landings do |t|
      t.text   :article   # 投稿記事 markdown形式

      t.timestamps
    end
  end
end
