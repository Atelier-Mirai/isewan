class CreateUsers < ActiveRecord::Migration[5.0]
  def change
    create_table :users do |t|
      t.string  :name
      t.string  :password_digest
      t.string  :remember_digest
      t.boolean :admin, default: false

      t.timestamps
    end

    add_index :users, [:remember_digest], name: "index_users_on_remember_digest", using: :btree
  end
end
