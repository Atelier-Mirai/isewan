class CreateWinners < ActiveRecord::Migration[5.0]
  def change
    create_table :winners do |t|
      t.integer :times
      t.string  :isewan_name
      t.string  :techno_name

      t.timestamps
    end
  end
end
