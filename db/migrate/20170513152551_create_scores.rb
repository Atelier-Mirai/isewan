class CreateScores < ActiveRecord::Migration[5.0]
  def change
    create_table :scores do |t|
      t.integer :year # 何年の大会か
      t.string  :provisional_or_decision # 暫定 確定
      t.text    :data # 成績データ(シリアライズして全部突っ込む)
      t.text    :info # 大会情報(開催日・場所など、いろいろ)
      t.text    :note # 凡例

      t.timestamps
    end
  end
end
