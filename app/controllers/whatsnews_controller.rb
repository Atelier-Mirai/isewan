class WhatsnewsController < ApplicationController
  before_action :set_whatsnews, only: [:update, :destroy]
  before_action :admin_user?

  def index
    @whatsnews = Whatsnew.all.rank(:row_order)
  end

  def lists
    @lists = Whatsnew.display_all.page(params[:page]).per(10)
    render 'lists'
  end

  def create
    @whatsnew = Whatsnew.new(whatsnews_params)
    @whatsnew.save
    redirect_back(fallback_location: root_path)
  end

  def update
    @whatsnews.update(whatsnews_params)
    redirect_to whatsnews_index_path
  end

  def destroy
    @whatsnews.destroy
    redirect_back(fallback_location: root_path)
  end

  # this action will be called via ajax
  def sort
    whatsnew = Whatsnew.find(params[:whatsnews_id])
    whatsnew.update(whatsnews_params)
    head :ok, content_type: "text/html"
  end

  private

  def admin_user?
    if current_user.nil? || !current_user.admin?
      redirect_to root_path
    end
  end

  def set_whatsnews
    @whatsnews = Whatsnew.find(params[:id])
  end

  def whatsnews_params
    params.require(:whatsnew).permit(:title, :article, :display, :row_order_position)
  end
end
