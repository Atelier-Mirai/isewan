class ViewersController < ApplicationController
  layout 'viewer'

  def show
    s3file = S3file.find_by(id: params[:id])
    @file_name = s3file.file_name
    @file_body = s3file.s3_aws_file_read

    case File.extname(s3file.file_name)
    when '.html'
      @file_body = @file_body.html_safe
    when '.txt', '.md'
      @file_body = markdown(@file_body)
    when '.doc', '.docx'
    # TODO: herokuにアップすると動かないので、コメントアウト
    #   File.open("public/#{@file_name}", "w") do |f| # wは書き込み権限
    #     f.puts @file_body
    #   end
    #   @file_body = markdown(WordToMarkdown.new("public/#{@file_name}").to_s)
    when '.xlsx'
      File.open("public/#{@file_name}", "w") do |f| # wは書き込み権限
        f.puts @file_body
      end

      # worksheetから読み込み、結果をtableへ格納
      workbook = RubyXL::Parser.parse("public/#{@file_name}")
      worksheet = workbook[0]
      table = []

      worksheet.each do |row|
        record = []
        row && row.cells.each_with_index do |cell|
          val = cell && cell.value
          if val.instance_of?(DateTime)
            val = val.strftime("%Y-%m-%d")
          end
          # record << val
          record << {
                      value:     val,
                      fontsize:  cell.font_size,
                      alignment: cell.horizontal_alignment,
                    }
        end
        table << record
      end

      total_column_presence    = []
      first_column_presence   = []

      table.each do |row|
        sum = 0
        row.each_with_index do |cell, column_index|
          if column_index == 0
            if cell[:value].present?
              first_column_presence << 1
            else
              first_column_presence << 0
            end
          end
          if cell[:value].present?
            sum += 1
          end
        end
        total_column_presence << sum
      end

      list_candidate = []
      total_column_presence.each_with_index do |e, i|
        if first_column_presence[i] == 0 && total_column_presence[i] != 0
          list_candidate << 1
        else
          list_candidate << 0
        end
      end

      table_candidate = []
      total_column_presence.each_with_index do |e, i|
        if first_column_presence[i] == 1 && total_column_presence[i] >= 2
          table_candidate << total_column_presence[i]
        else
          table_candidate << 0
        end
      end

      count = 0
      table_candidate.each_with_index do |e, i|
        if i == 0
          if e >= 2
            count = 1
          else
            count = 0
          end
          next
        end
        if e == 0
          if count == 1
            # 1行しかなければ削除
            table_candidate[i-1] = 0
            count == 0
          end
        else
          count += 1
        end
      end
      # 最後に1行だけあった場合も削除
      if table_candidate[-1] >= 2 && table_candidate[-2] == 0
        table_candidate[-1] = 0
      end

      # theadとtbodyの区別をする
      # table_candidate[i] 負数: thead 正数: tbody 零: tableではない
      count = 0
      table_candidate.each_with_index do |e, i|
        if e >= 2 && count == 0
          count = 1
          table_candidate[i] *= -1 # 符号反転 table header の意味
        elsif e >= 2 && count == 1
          # 何もしない
        elsif e == 0
          count = 0
        end
      end

      paragraph_candidate = []
      count = 0
      first_column_presence.each_with_index do |e, i|
        if i == 0
          paragraph_candidate[i] = 0
          next
        end
        if e == 1 && count == 0
          count = 1
          paragraph_candidate[i] = 0
          next
        end
        if e == 1 && count == 1
          paragraph_candidate[i] = 1
        end
        if e == 0
          count = 0
          paragraph_candidate[i] = 0
        end
      end
      paragraph_candidate.each_with_index do |e, i|
        if table_candidate[i] != 0
          paragraph_candidate[i] = 0
        end
      end

      header_candidate = []
      first_column_presence.each_with_index do |e, i|
        if e == 0
          header_candidate[i] = 0
        elsif e == 1
          if list_candidate[i] == 0 && table_candidate[i] == 0 && paragraph_candidate[i] == 0
            # セルのフォントサイズを見る
            case table[i][0][:fontsize]
            when 18 .. Float::INFINITY
              header_candidate[i] = 1 # h1
            when 12 ... 18
              header_candidate[i] = 2 # h2
            else
              header_candidate[i] = 3 # h3
            end
          else
            header_candidate[i] = 0
          end
        end
      end

      # 候補の抽出が完了したので、markdownを作成する
      md = ""
      table_flag = false
      table.each_with_index do |row, i|
        # mode 選定
        mode = "h#{header_candidate[i]}".to_sym  if header_candidate[i]    >  0
        mode = :list                             if list_candidate[i]      == 1
        mode = :paragraph                        if paragraph_candidate[i] == 1
        mode = :thead                            if table_candidate[i]     <  0
        mode = :tbody                            if table_candidate[i]     >  0

        md << row_join(row, mode:mode)
      end

      @file_body = markdown(md)

    when '.pdf'
      File.open("public/#{@file_name}", "w") do |f| # wは書き込み権限
        f.puts @file_body
      end
      redirect_to pdfjs.minimal_path(file: "/#{@file_name}")
    end
  end

  private
    def row_join(row, mode:nil)
      mark = ""
      case mode
      when :h1;        mark = "# "
      when :h2;        mark = "## "
      when :h3;        mark = "### "
      when :list;      mark = "- "
      when :paragraph; mark = ""
      when nil;        mark = ""
      end

      if mode.in? ( %i(h1 h2 h3 list paragraph) << nil )
        md = mark
        row.each do |cell|
          md << cell[:value].to_s
        end
        md << "\n"

      elsif mode.in? %i(thead tbody)
        md = ""
        case mode
        when :thead
          row.each do |cell|
            md << "|" << cell[:value].to_s
          end
          md << "|\n"

          delimiter = ""
          row.each do |cell|
            case cell[:alignment]
            when nil, "left"
              delimiter << "|:---"
            when "center"
              delimiter << "|:---:"
            when "right"
              delimiter << "|---:"
            end
          end
          delimiter << "|"

          md << delimiter << "\n"
        when :tbody
          row.each do |cell|
            md << "|" << cell[:value].to_s
          end
          md << "|\n"
        end
      end
      md
    end
end
