class WinnersController < ApplicationController
  before_action :set_winner, only: [:update, :destroy]
  before_action :admin_user?, except: [:index]

  def index
    @winners = Winner.all
  end

  def edit
    @winners = Winner.all
    @winner  = Winner.new(times: Winner.maximum(:times).succ)
  end

  def create
    @winner = Winner.new(winner_params)
    @winner.save
    redirect_to edit_winner_path(1)
  end

  def update
    @winner.update(winner_params)
    redirect_to edit_winner_path(1)
  end

  def destroy
    @winner.destroy
    redirect_to edit_winner_path(1)
  end

  private
    def admin_user?
      if current_user.nil? || !current_user.admin?
        redirect_to root_path
      end
    end

    def set_winner
      @winner = Winner.find(params[:id])
    end

    def winner_params
      params.require(:winner).permit(:times, :isewan_name, :techno_name)
    end
end
