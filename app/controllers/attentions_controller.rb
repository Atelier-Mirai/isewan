class AttentionsController < ApplicationController
  before_action :set_attention
  before_action :admin_user?

  def edit
  end

  def update
    # 添付ファイル確認
    if params[:upload_file].present?
      filename  = params[:upload_file].original_filename
      extension = File.extname(filename)
      if extension.downcase.in? [".gif", ".jpeg", ".jpg", ".png"]
        # 添付ファイルが画像だったら
        params[:attention][:picture] = params[:upload_file]
        params[:attention][:display] = true
      end
    end

    @attention.update(attention_params)
    redirect_to edit_attention_path
  end

  def destroy
    # TODO: 本当はアップした写真もきちんと消すべきだが・・・
    @attention.update_columns(picture: nil, caption: nil)
    redirect_to edit_attention_path
  end

  private

  def admin_user?
    if current_user.nil? || !current_user.admin?
      redirect_to root_path
    end
  end

  def set_attention
    @attention = Attention.first
  end

  def attention_params
    params.require(:attention).permit(:title, :article, :picture, :caption, :display)
  end
end
