class GalleriesController < ApplicationController
  before_action :set_gallery, only: [:update, :destroy]
  before_action :admin_user?

  def create
    gallery = params[:controller].singularize
    if params[:upload_file].present?
      filename  = params[:upload_file].original_filename
      extension = File.extname(filename)
      if extension.downcase.in? [".gif", ".jpeg", ".jpg", ".png"]
        params[gallery][:picture] = params[:upload_file]
        params[gallery][:display] = true
        @gallery = gallery.classify.constantize.new(gallery_params)
      end
    end
    @gallery.save
    redirect_to polymorphic_path(gallery.pluralize)
  end

  def update
    @gallery.update(gallery_params)
    redirect_to polymorphic_path(@gallery.class.name.underscore.pluralize)
  end

  def destroy
    @gallery.destroy
    redirect_to polymorphic_path(@gallery.class.name.underscore.pluralize)
  end

  # this action will be called via ajax
  def sort
    gallery = Gallery.find(params[:gallery_id])
    gallery.update(gallery_params(gallery))
    head :ok, content_type: "text/html"
  end

  private

  def admin_user?
    if current_user.nil? || !current_user.admin?
      redirect_to root_path
    end
  end

  def set_gallery
    @gallery = Gallery.find(params[:id])
  end

  # this action will be called via ajax
  def gallery_params(gallery = nil)
    model_name = if gallery.present?
                   # ranked-model による sort 用
                   gallery.class.name.underscore.to_sym
                 else
                   # 通常のCRUD用
                   self.class.name.underscore.split('_')[0].singularize.to_sym
                 end
    puts model_name
    params.require(model_name).permit(:picture, :caption, :url, :display, :row_order_position)
  end
end
