class EventsController < ApplicationController
  before_action :set_events, only: [:update, :destroy]
  before_action :admin_user?

  # def new
  #   @event = Event.new
  #   @event.s3files.build
  # end
  #
  def create
    event = params[:controller].singularize
    @event = event.classify.constantize.new(event_params(event))
    @event.save

    # 添付ファイルがあった場合の処理
    # attached_file = params[event][:s3files_attributes]["0"][:file]
    attached_file = params[:upload_file]
    raise
    if attached_file.present?
      @event.s3files[0].s3_aws_file_upload(attached_file)
    end
    redirect_to polymorphic_path(event.pluralize)
  end

  def update
    @event.update(event_params)
    # 添付ファイルがあった場合の処理
    attached_file = params[:upload_file]
    if attached_file.present?
      @event.s3files[0].s3_aws_file_replace(attached_file)
    end
    redirect_to polymorphic_path(@event.class.name.underscore.pluralize)
  end

  def destroy
    @event.destroy
    redirect_to polymorphic_path(@event.class.name.underscore.pluralize)
  end

  private

  def admin_user?
    if current_user.nil? || !current_user.admin?
      redirect_to root_path
    end
  end

  def set_events
    @event = Event.find(params[:id])
  end

  # edit など
  def event_params(event = nil)
    model_name = if event.nil?
                   @event.class.name.underscore.to_sym
                 else
                   event.to_sym
                 end
    # params.require(model_name).permit(:title, :article, :display,
    #                               :area, :schedule_adjustment, :start_date, :end_date,
    #                               :name, :place, :contact, :phone, :url, :section,
    #                               :upload_file,
    #                               s3files_attributes: [:id, :title, :file, :file_name, :comment, :event_id, :_destroy]
    #                               )
    params.require(model_name).permit(:title, :article, :display,
                                  :area, :schedule_adjustment, :start_date, :end_date,
                                  :name, :place, :contact, :phone, :url, :email, :type,
                                  :upload_file,
                                  s3files_attributes: [:id, :title, :file, :file_name, :comment, :event_id, :_destroy]
                                  )
  end
end
