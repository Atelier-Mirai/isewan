class PhotosController < GalleriesController
  def index
    @type      = '写真'
    @galleries = Photo.all.rank(:row_order)
    @gallery   = Photo.new
    @links     = PhotoLink.rank(:row_order)
    @link      = PhotoLink.new
  end
end
