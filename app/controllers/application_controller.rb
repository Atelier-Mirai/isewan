class ApplicationController < ActionController::Base
  include ApplicationHelper
  include SessionsHelper
  include MarkdownHelper

  protect_from_forgery with: :exception
  after_action  :cors

  if Rails.env.production?
    rescue_from Exception, with: :error500
    rescue_from ActiveRecord::RecordNotFound, ActionController::RoutingError, with: :error404
  end

  private

  def error404(e)
    render 'error404', status: 404, formats: [:html]
  end

  def error500(e)
    render 'error500', status: 500, formats: [:html]
  end

  def cors
    response.headers['Access-Control-Allow-Origin']      = '*'
    response.headers['Access-Control-Allow-Methods']     = 'POST, GET, PUT, DELETE, OPTIONS, HEAD'
    response.headers['Access-Control-Allow-Credentials'] = 'true'
    response.headers['Access-Control-Allow-Headers']     = 'X-PINGOTHER'
    response.headers['Access-Control-Max-Age']           = '86400' # 24 hours
  end
end
