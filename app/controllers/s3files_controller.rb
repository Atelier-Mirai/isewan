class S3filesController < ApplicationController
  before_action :admin_user?

  def destroy
    @s3file = S3file.find(params[:id])
    @s3file.s3_aws_file_remove
    # @s3file.destroy
    @s3file.update_columns(title: "", file_name: nil, comment: "")

    redirect_to url_for controller: params[:format].pluralize, action: :index
  end

  private
    def admin_user?
      if current_user.nil? || !current_user.admin?
        redirect_to root_path
      end
    end
end
