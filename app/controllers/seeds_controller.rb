class SeedsController < ApplicationController
  before_action :set_seed, only: [:edit, :update, :destroy]
  before_action :admin_user?, except: [:show]

  # GET /seeds
  def index
    @seeds = Seed.all.order(year: :desc)
    @seed  = Seed.new
  end

  # GET /seeds/1
  def show
    @seed = Seed.find_by(year: params[:id])
  end

  # GET /seeds/new
  def new
    @seed = Seed.new
  end

  # GET /seeds/1/edit
  def edit
  end

  # POST /seeds
  def create
    @seed = Seed.create(seed_params)
    redirect_to seeds_path
  end

  # PATCH/PUT /seeds/1
  def update
    @seed.update(seed_params)
    redirect_to seeds_path
  end

  # DELETE /seeds/1
  def destroy
    @seed.destroy
    redirect_to seeds_path
  end

  private
    def admin_user?
      if current_user.nil? || !current_user.admin?
        redirect_to root_path
      end
    end

    # Use callbacks to share common setup or constraints between actions.
    def set_seed
      @seed = Seed.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def seed_params
      params.require(:seed).permit(:year,
                                   :article,
                                   :isewan_seeds,
                                   :isewan_male,
                                   :isewan_female,
                                   :techno_seeds,
                                   :techno_male,
                                   :techno_female)
    end
end
