class PdfsController < ApplicationController
  before_action :set_pdfs, only: [:update, :destroy]
  before_action :admin_user?

  def index
    @pdfs  = S3file.all.select { |s3file| s3file.file_name.present? }
  end

  # def new
  #   @pdf = pdf.new
  #   @pdf.s3files.build
  # end
  #
  def create
    # pdf = params[:controller].singularize
    # @pdf = pdf.classify.constantize.new(pdf_params(pdf))
    # @pdf.save

    # 添付ファイルがあった場合の処理
    # attached_file = params[pdf][:s3files_attributes]["0"][:file]
    attached_file = params[:upload_file]
    if attached_file.present?
      s3file = S3file.create(title: "", file_name: params[:upload_file].original_filename, comment: "", event_id: 16)
      s3file.s3_aws_file_upload(attached_file)
    end
    redirect_to pdfs_path
  end

  def update
    @pdf.update(pdf_params)
    # 添付ファイルがあった場合の処理
    attached_file = params[:upload_file]
    if attached_file.present?
      @pdf.s3files[0].s3_aws_file_replace(attached_file)
    end
    redirect_to polymorphic_path(@pdf.class.name.underscore.pluralize)
  end

  def destroy
    @pdf.destroy
    redirect_to polymorphic_path(@pdf.class.name.underscore.pluralize)
  end

  private

  def admin_user?
    if current_user.nil? || !current_user.admin?
      redirect_to root_path
    end
  end

  def set_pdfs
    @pdf = pdf.find(params[:id])
  end

  # edit など
  def pdf_params
    params.require(:pdf).permit(:title, :article, :display,
                                  :area, :schedule_adjustment, :start_date, :end_date,
                                  :name, :place, :contact, :phone, :url, :email, :type,
                                  :upload_file,
                                  s3files_attributes: [:id, :title, :file, :file_name, :comment, :pdf_id, :_destroy]
                                  )
  end
end
