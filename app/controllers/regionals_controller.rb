class RegionalsController < EventsController
  def index
    @events = Regional.all
    @event  = Regional.new
    @event.s3files.build

    @locals = { h1: "愛知の大会", h2: "", link_text: '愛知の大会 の編輯を終える', link_path: "#{root_path}#regional" }
  end
end
