class MoviesController < GalleriesController
  def index
    @type      = '動画'
    @galleries = Movie.all.rank(:row_order)
    @gallery   = Movie.new
    @links     = MovieLink.rank(:row_order)
    @link      = MovieLink.new
  end
end
