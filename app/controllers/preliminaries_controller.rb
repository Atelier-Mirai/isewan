class PreliminariesController < EventsController
  def index
    @events = Preliminary.all
    @event  = Preliminary.new
    @event.s3files.build
    @locals = { h1: "伊勢湾カップ #{Date.today.year}", h2: "予選大会" }
  end
end
