class WelcomeController < ApplicationController
  def index
    @landing            = Landing.first
    @intro              = Intro.first
    @whatsnews          = Whatsnew.display_all.rank(:row_order)
    @scores             = Score.all.pluck(:year)
    @attention          = Attention.display_all.first

    @photos             = Photo.display_all.rank(:row_order)
    @movies             = Movie.display_all.rank(:row_order)
    @photo_links        = PhotoLink.rank(:row_order)
    @movie_links        = MovieLink.rank(:row_order)

    @preliminary_events = Preliminary.display_all
    @preliminary_locals = { h1: "予選大会" }
    @regional_events    = Regional.display_all
    @regional_locals    = { h1: "愛知の大会" }
  end
end
