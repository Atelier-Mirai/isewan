class LandingsController < ApplicationController
  before_action :set_landing
  before_action :admin_user?

  def edit
  end

  def update
    @landing.update(landing_params)
    redirect_to edit_landing_path
  end

  private
    def admin_user?
      if current_user.nil? || !current_user.admin?
        redirect_to root_path
      end
    end

    def set_landing
      @landing = Landing.first
    end

    def landing_params
      params.require(:landing).permit(:article)
    end
end
