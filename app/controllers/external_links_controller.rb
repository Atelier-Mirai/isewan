class ExternalLinksController < ApplicationController
  before_action :set_link, only: [:update, :destroy]
  before_action :admin_user?

  def create
    link  = params[:controller].singularize
    @link = link.classify.constantize.new(link_params)
    @link.save
    redirect_back(fallback_location: root_path)
  end

  def update
    @link.update(link_params)
    redirect_back(fallback_location: root_path)
  end

  def destroy
    @link.destroy
    redirect_back(fallback_location: root_path)
  end

  # this action will be called via ajax
  def sort
    link = ExternalLink.find(params[:external_link_id])
    link.update(link_params(link))
    head :ok, content_type: "text/html"
  end

  private

  def admin_user?
    if current_user.nil? || !current_user.admin?
      redirect_to root_path
    end
  end

  def set_link
    @link = ExternalLink.find(params[:id])
  end

  # this action will be called via ajax
  def link_params(link = nil)
    model_name = if link.present?
                   # ranked-model による sort 用
                   link.class.name.underscore.to_sym
                 else
                   # 通常のCRUD用
                   :link
                 end
    params.require(model_name).permit(:text, :url, :row_order_position)
  end
end
