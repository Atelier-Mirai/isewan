class IntrosController < ApplicationController
  before_action :set_intro
  before_action :admin_user?

  def edit
  end

  def update
    @intro.update(intro_params)
    redirect_to edit_intro_path
  end

  private
    def admin_user?
      if current_user.nil? || !current_user.admin?
        redirect_to root_path
      end
    end

    def set_intro
      @intro = Intro.first
    end

    def intro_params
      params.require(:intro).permit(:article)
    end
end
