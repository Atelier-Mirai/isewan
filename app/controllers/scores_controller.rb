class ScoresController < ApplicationController
  before_action :set_score, only: [:edit, :update, :destroy]
  before_action :admin_user?, except: [:show]

  # GET /scores
  def index
    @scores = Score.all
    @score  = Score.new
  end

  # GET /scores/1
  def show
    @score = Score.find_by(year: params[:id])
  end

  # GET /scores/new
  def new
    @score = Score.new
  end

  # GET /scores/1/edit
  def edit
  end

  # POST /scores
  def create
    @score = Score.new(score_params)
    @score.save

    data  = params[:score][:data]
    table = read_excel_attached_file(data)
    if table.present?
      @score.update_columns(data: table)
    end
    redirect_to scores_path
  end

  # PATCH/PUT /scores/1
  def update
    # 添付ファイル確認
    # data  = params[:score][:data]
    data  = params[:upload_file]
    table = read_excel_attached_file(data)

    @score.update(score_params)
    @score.update_columns(data: table) if table.present?
    redirect_to scores_path
  end

  # DELETE /scores/1
  def destroy
    @score.destroy
    redirect_to scores_path
  end

  private

  def admin_user?
    if current_user.nil? || !current_user.admin?
      redirect_to root_path
    end
  end

  # Use callbacks to share common setup or constraints between actions.
  def set_score
    @score = Score.find(params[:id])
  end

  # Only allow a trusted parameter "white list" through.
  def score_params
    params.require(:score).permit(:year, :provisional_or_decision, :info, :note)
  end
end
