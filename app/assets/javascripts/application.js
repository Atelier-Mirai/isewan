/*
*= require 'jquery3'
*= require 'jquery_ujs'
*= require 'tether'
*= require 'bootstrap'
*= require 'animatescroll'
*= require 'hammer.min'
*= require 'jquery.tosrus.all.min'
*= require 'FlameViewportScale'
*= require 'tablesorter'
*= require 'select2-full'
*= require 'custom_file_input'
*= require 'jquery.recommented_browsers'
*= require_tree .
*= require_self
*/

//= require jquery-ui

$(function(){
  // テーブル並び替え
  $('table').tablesorter();

  // ナビバー　スクロール
  // http://v4-alpha.getbootstrap.com/components/scrollspy/
  // http://bootstrap3.cyberlab.info/sample/javascript-scrollspy-navbar.html#sampleA
  $('.page-scroll').click( function () {
    var hrefValue = $(this).attr('href');
    $(hrefValue).animatescroll({ padding: 66 });
  });
  // デートピッカー
  $('.datepicker').datepicker();

  // セレクトボックス
  $(".searchable").select2({
    width:      85,   //  横幅
    allowClear: false  //  Clear x を表示しない
  });

  // 推奨ブラウザ
  $("#recommend").recommended_browsers();
  // IE用動画リンク
  // $("#movie-link-for-ie").movie_link_for_ie();
});
