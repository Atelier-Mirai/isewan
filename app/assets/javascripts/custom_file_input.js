(function ($) {
  var doc = document;
  var supportsMultipleFiles = "multiple" in doc.createElement("input");

  // photo
  $(doc).on("change", ".custom-file > input[type=file]", function () {
    var input = this,
    fileNames = [],
    label = input.nextElementSibling,
    files, len, i = -1, labelValue;

    if (supportsMultipleFiles) {
      len = (files = input.files).length;
      while (++i < len) {
        fileNames.push(files[i].name);
      }
    } else {
      fileNames.push(input.value.replace(/\\/g, "/").replace(/.*\//, "")); // Removes the path info ("C:\fakepath\" or sth like that)
    }

    label.textContent = labelValue = fileNames.length === 0 ? "" : fileNames.join(", ");
    label.setAttribute("title", labelValue);
  });

  // attached_file
  $(doc).on("change", ".custom-file input[type=file]", function () {
    var input = this,
    fileNames = [],
    label = input.parentElement.nextElementSibling,
    files, len, i = -1, labelValue;

    if (supportsMultipleFiles) {
      len = (files = input.files).length;
      while (++i < len) {
        fileNames.push(files[i].name);
      }
    } else {
      fileNames.push(input.value.replace(/\\/g, "/").replace(/.*\//, "")); // Removes the path info ("C:\fakepath\" or sth like that)
    }

    label.textContent = labelValue = fileNames.length === 0 ? "" : fileNames.join(", ");
    label.setAttribute("title", labelValue);
  });
})(jQuery);
