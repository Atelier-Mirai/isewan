(function($) {
  $.fn.movie_link_for_ie = function() {
    var str = "";
    if (navigator.userAgent.search('WebKit') == -1 && navigator.userAgent.search('Firefox') == -1 && navigator.userAgent.search('Opera') == -1) {
      str = '<div><p style=margin-bottom:0>Internet Explorer など古いブラウザで再生ボタンが利かない場合は、左上の「伊勢湾カップ」を押すと視聴できます。</p></div>';
      return this.each(function(){
        $(this).prepend(str).css({
           'margin': '1rem auto',
           'background': 'white',
           'color': 'black',
           'padding': '1rem',
           'border': 'double 1px #cc8800',
           'text-align': 'center',
         });
      });
    }; // end of if
  }
})(jQuery);

// str = '<div id=movie-link-for-ie><p style=margin-bottom:0>Internet Explorer など古いブラウザをお使いの方は、次のリンクから動画をご覧ください。<br><a href=https://vimeo.com/285622712>2018年大会</a> <a href=https://vimeo.com/228689575>2017年大会1日目</a> <a href=https://youtu.be/e-djU8JIetg>2017年大会2日目</a> <a href=https://vimeo.com/179582374>2016年大会</a> <a href=https://youtu.be/wKy-RSIdEWw>2015年大会</a></p></div>';
