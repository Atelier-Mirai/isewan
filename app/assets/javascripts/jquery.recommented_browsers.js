(function($) {
  $.fn.recommended_browsers = function() {
    var str = "";
    if (navigator.userAgent.search('WebKit') == -1 && navigator.userAgent.search('Firefox') == -1 && navigator.userAgent.search('Opera') == -1) {
      str = '<div id=recommend><p>Internet Explorer など古いブラウザでは快適に閲覧できないことがあります。<br><a href="https://www.google.co.jp/chrome/">Google Chrome</a> など、最新のブラウザがお薦めです。</p></div>';
      return this.each(function(){
        $(this).prepend(str);
        $('#recommend').css({
           'background': 'white',
           'color': 'black',
           'font-size': 'medium',
           'font-weight': 'bold',
           'margin-top': '66px',
           'padding-top': '1em',
           'height': '5em',
           'border': 'double 1px #cc8800',
           'text-align': 'center',
         });
      });
    }; // end of if
  }
})(jQuery);
