class Event < ApplicationRecord
  # self.inheritance_column = :_type_disabled

  has_many :s3files, dependent: :destroy, inverse_of: :event
  accepts_nested_attributes_for :s3files, allow_destroy: true

  scope :display_all, -> { where(display: true) }
  default_scope -> { order(area: :asc, start_date: :asc, end_date: :asc) }

end
