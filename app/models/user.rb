class User < ApplicationRecord
  attr_accessor :remember_token
  validates :name, presence: true,
                   length: { maximum: 50 },
                   uniqueness: { case_sensitive: true }
  validates :password, presence: true, allow_nil: false
  has_secure_password

  class << self
    # ランダムなトークンを返す
    def new_token
      SecureRandom.urlsafe_base64
    end

    # 与えられた文字列のハッシュ値を返す
    def digest(string)
      cost = ActiveModel::SecurePassword.min_cost ? BCrypt::Engine::MIN_COST
                                                  : BCrypt::Engine.cost
      BCrypt::Password.create(string, cost: cost)
    end
  end

  def authenticated?(attribute, token)
    digest = send("#{attribute}_digest")
    return false if digest.nil?
    BCrypt::Password.new(digest).is_password?(token)
  end

  def remember
    self.remember_digest = User.new_token
    update_attribute(:remember_digest, User.digest(remember_token))
  end

  # ユーザーログインを破棄する
  def forget
    update_attribute(:remember_digest, nil)
  end
end
