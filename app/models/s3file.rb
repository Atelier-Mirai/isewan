class S3file < ApplicationRecord
  # https://joppot.info/2014/06/13/1583
  # railsのaws-sdk gemを使ってs3に画像ファイルをフォームからアップロードする
  # AWS SDK for Ruby V2でS3をいろいろ使う
  # http://qiita.com/tamikura@github/items/a6f819a0ce2b6c79bec7

  before_destroy :s3_aws_file_remove
  belongs_to :event

  attr_accessor :file
  # event には、空でもいいので、添付ファイルのレコードが存在するようにする
  # （後から添付ファイルの追加ができるように)
  # validates :file_name, presence: true,
                        # uniqueness: { case_sensitive: false } # 大文字小文字を区別しない

  # S3インスタンス生成
  S3 = Aws::S3::Client.new(
    access_key_id:     'AKIAJG3QOH5BY4UQGVQA',
    secret_access_key: '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',
    region:            'ap-northeast-1',
  )

  def s3_aws_file_read
    # s3より取得
    S3.get_object(
      bucket: 'isewan-bucket',
      key:    self.file_name,
    ).body.read
  end

  def s3_aws_file_upload(attached_file)
    attached_file_name = attached_file.original_filename.downcase
    # s3へアップロード
    S3.put_object(
      bucket: 'isewan-bucket',
      key:    attached_file_name,
      body:   attached_file,
    )
    # s3 model 更新
    self.update_columns(file_name: attached_file_name)
  end

  # TODO: ファイル名が同じで中身が違う場合に、置き換わらない
  def s3_aws_file_replace(attached_file)
    attached_file_name = attached_file.original_filename.downcase
    # 元のファイルを削除する
    self.s3_aws_file_remove
    # s3へアップロード
    S3.put_object(
      bucket: 'isewan-bucket',
      key:    attached_file_name,
      body:   attached_file,
    )
    # s3 model 更新
    self.update_columns(file_name: attached_file_name)
  end

  def s3_aws_file_remove
    # s3上のファイル削除
    if self.file_name.present?
      S3.delete_object(:bucket => 'isewan-bucket', :key => self.file_name)
    end
  end
end
