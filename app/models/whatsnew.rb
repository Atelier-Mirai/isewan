class Whatsnew < ApplicationRecord
  include RankedModel
  ranks :row_order

  scope :display_all, -> { where(display: true) }
end
