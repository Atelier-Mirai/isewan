class Score < ApplicationRecord
  default_scope -> { order(year: :desc) }

  serialize :data
  serialize :info
  serialize :note
end
