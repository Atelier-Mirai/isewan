class Winner < ApplicationRecord
  default_scope -> { order(times: :asc) }
  validates :times, presence: true, uniqueness: true
end
