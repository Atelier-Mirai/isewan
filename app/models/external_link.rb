class ExternalLink < ApplicationRecord
  # self.inheritance_column = :_type_disabled

  include RankedModel
  ranks :row_order

  validates :text, presence: true
  validates :url,  presence: true
end
