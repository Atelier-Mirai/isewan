class Attention < ApplicationRecord
  scope :display_all, -> { where(display: true) }

  mount_uploader :picture, PictureUploader
  validate :picture_size

  # アップロード画像のサイズを検証する
  def picture_size
    if picture.size > 5.megabytes
      errors.add(:picture, "5MBより小さい画像をアップして下さい")
    end
  end
end
