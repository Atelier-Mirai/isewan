# 段級
class Dankyu
  attr_accessor :rating, :dankyu, :rank

  RANK_HASH = { 2900 => ['八段', -7],
                2700 => ['七段', -6],
                2500 => ['六段', -5],
                2300 => ['五段', -4],
                2100 => ['四段', -3],
                1900 => ['三段', -2],

                1700 => ['二段', -1],

                1550 => ['初段',  0],

                1450 => ['１級',  1],
                1350 => ['２級',  2],
                1250 => ['３級',  3],

                1150 => ['４級',  4],
                1050 => ['５級',  5],

                950 => ['６級',  6],
                850 => ['７級',  7],

                750 => ['８級',  8],
                650 => ['９級',  9],

                550 => ['10級', 10],
                450 => ['11級', 11],

                350 => ['12級', 12],
                250 => ['13級', 13],

                150 => ['14級', 14],
                50 => ['15級', 15],
                0 => ['初心者', 16] }.freeze

  def initialize(rating_or_dankyu)
    if rating_or_dankyu.numeric?
      @rating = rating_or_dankyu.to_i
      RANK_HASH.each do |key, value|
        next unless @rating >= key
        @dankyu = value[0]
        @rank   = value[1]
        break
      end
    else
      @dankyu = rating_or_dankyu
      RANK_HASH.each do |key, value|
        next unless @dankyu == value[0]
        @rating = key
        @rank   = value[1]
        break
      end
    end
  end

  def self.handicap(score1, score2)
    @player1 = Dankyu.new(score1)
    @player2 = Dankyu.new(score2)

    # Rの大きい方を、player1に
    @player1, @player2 = @player2, @player1 if @player1.rating < @player2.rating

    difference = @player2.rank - @player1.rank
    difference -= 1 if @player1.rank <= 6 # 上級会員特別ルール
    difference = 0  if difference < 0

    teai = if difference >= 11 && @player2.rank >= 14 then '六枚落'
           elsif difference >=  9 then '四枚落'
           elsif difference >=  7 then '二枚落'
           elsif difference >=  6 then '飛香落'
           elsif difference >=  5 then '飛落'
           elsif difference >=  4 then '角落'
           elsif difference >=  3 then '香落'
           else                        '平手'
           end

    [@player2, @player1, difference, teai]
  end
end
