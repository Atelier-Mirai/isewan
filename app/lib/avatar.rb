# アバター と 名前表示 ユーザープロフィールへのリンク
module Avatar
  # avatar_only: true なら、アバターのみ表示
  def avatar_link_to(user, size: 48, avatar_only: false)
    # id 見つからなければ、ゲストユーザー生成
    if user.nil? || user.is_a?(Integer)
      user = User.find_by(id: user) || GuestUser.new
    end

    image_src = case user&.role
                # 運営
                when 'admin'
                  'fu.png'
                # 登録ユーザ
                when 'committee', 'member'
                  gravatar_id  = Digest::MD5.hexdigest(user&.email || "")
                  gravatar_url = "https://secure.gravatar.com/avatar/#{gravatar_id}?s=#{size}"
                # ゲストユーザー
                when 'guest'
                  'fu.png'
                end

    name = user&.role == 'admin' ? '運営' : user&.name
    avatar = image_tag(image_src, alt: name, size: "#{size}x#{size}", class: 'gravatar')

    # アバターのみ返す
    return avatar if avatar_only
    # ゲスト、運営
    return "#{avatar} #{name}".html_safe if user.role.in? ['guest', 'admin']
    # 登録ユーザー
    # link_to "#{avatar} #{name}".html_safe, view_context.user_profile_path(user), class: 'avatar-link'
    link_to "#{avatar} #{name}".html_safe, "/users/#{user.id}/profile", class: 'avatar-link'
  end
end
