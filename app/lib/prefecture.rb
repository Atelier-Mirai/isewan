# 都道府県
class Prefecture
  attr_accessor :code, :name, :name_e

  KANJI_REGEXP = /[一-龠々]/
  NAMES = {
    '北海道' => 'hokkaido',
    '青森' => 'aomori',
    '岩手' => 'iwate',
    '宮城' => 'miyagi',
    '秋田' => 'akita',
    '山形' => 'yamagata',
    '福島' => 'fukushima',
    '茨城' => 'ibaraki',
    '栃木' => 'tochigi',
    '群馬' => 'gunma',
    '埼玉' => 'saitama',
    '千葉' => 'chiba',
    '東京' => 'tokyo',
    '神奈川' => 'kanagawa',
    '新潟' => 'niigata',
    '富山' => 'toyama',
    '石川' => 'ishikawa',
    '福井' => 'fukui',
    '山梨' => 'yamanashi',
    '長野' => 'nagano',
    '岐阜' => 'gifu',
    '静岡' => 'shizuoka',
    '愛知' => 'aichi',
    '三重' => 'mie',
    '滋賀' => 'shiga',
    '京都' => 'kyoto',
    '大阪' => 'osaka',
    '兵庫' => 'hyogo',
    '奈良' => 'nara',
    '和歌山' => 'wakayama',
    '鳥取' => 'tottori',
    '島根' => 'shimane',
    '岡山' => 'okayama',
    '広島' => 'hiroshima',
    '山口' => 'yamaguchi',
    '徳島' => 'tokushima',
    '香川' => 'kagawa',
    '愛媛' => 'ehime',
    '高知' => 'kochi',
    '福岡' => 'fukuoka',
    '佐賀' => 'saga',
    '長崎' => 'nagasaki',
    '熊本' => 'kumamoto',
    '大分' => 'oita',
    '宮崎' => 'miyazaki',
    '鹿児島' => 'kagoshima',
    '沖縄' => 'okinawa',
    '海外' => 'oversea'
  }.freeze

  def initialize(arg)
    if arg.is_a?(Integer)
      if arg.in?(1..48)
        @code = arg
        @name = NAMES.keys[arg - 1]
        @name_e = NAMES[@name]
      end
    else
      if arg.match?(KANJI_REGEXP)
        if NAMES.key?(arg)
          @code = NAMES.keys.index(arg)&.succ
          @name = arg
          @name_e = NAMES[arg]
        end
      elsif arg.match?(/[a-zA-Z]/)
        if NAMES.value?(arg)
          @code = NAMES.values.index(arg)&.succ
          @name = NAMES.key(arg)
          @name_e = arg
        end
      end
    end
  end

  def east?
    code <= 19 # 山梨
  end

  def west?
    code >= 20 # 長野
  end

  def east_west
    # 山梨まで東日本 長野から西日本
    code <= 19 ? :east : :west
  end
end
