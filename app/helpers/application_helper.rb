module ApplicationHelper
  include HtmlBuilder

  def html_br(str)
    h(str).gsub(/(\r\n?)|(\n)/, "<br>").html_safe
  end

  def link_to_top
    link_to '先頭へ戻る', '#landing', class: "page-scroll btn btn-outline-primary btn-sm"
  end

  def isewan_title(year)
    "第#{year-1982}回 伊勢湾カップ #{year}"
  end

  def link_to_top_page_with_copyright
    link_to "Copyright &copy; 1983-#{Time.zone.now.year.to_s} 伊勢湾カップ実行委員会 All rights reserved.".html_safe, 'https://isewan.herokuapp.com'
  end

  def read_excel_attached_file (data)
    table = []
    if data.present?
      filename  = data.original_filename
      extension = File.extname(filename)
      # attached_file.original_filename.downcase
      if extension.downcase.in? [".xlsx"]
        # 添付ファイルがエクセルだったら
        file = data
        name = file.original_filename

        File.open("public/#{name}", "wb") do |f| # wは書き込み権限
          f.write file.read
        end

        # worksheetから読み込み、結果をtableへ格納
        workbook = RubyXL::Parser.parse("public/#{name}")
        worksheet = workbook[0]

        worksheet.each do |row|
          record = []
          row && row.cells.each_with_index do |cell|
            val = cell && cell.value
            if val.instance_of?(DateTime)
              val = val.strftime("%Y-%m-%d")
            end
            record << val
          end
          table << record
        end
        if File.exist? "public/#{name}"
          File.delete "public/#{name}"
        end
      end
    end

    table.present? ? table : nil
  end
end
