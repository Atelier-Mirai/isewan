class EventPresenter < ModelPresenter
  delegate  :id,
            :display,
            :created_at,
            :updated_at,
            :area,
            :schedule_adjustment,
            :start_date,
            :end_date,
            :name,
            :place,
            :contact,
            :phone,
            :url,
            :email,
            :section,
            to: :object

  def mdw(date)
    date.strftime("%-m/%-d") + "(" + %w(日 月 火 水 木 金 土)[date.wday] + ")"
  end

  def decorated_dates(start_date, end_date, schedule_adjustment)
    return schedule_adjustment if schedule_adjustment.present?

    str = mdw(start_date) if start_date
    str << "〜#{mdw(end_date)}" if end_date
    return str
  end

  def decorated_contact(contact, url)
    url.present? ? link_to(contact, url) : contact
  end

  def tel_to(phone)
    if phone&.match?(/\d+-?\d+-?\d+/)
      link_to(phone, "tel:#{phone}")
    else
      phone
    end
  end

  def mail_to(email)
    valid_email_regex =/\A[\w+\-]+(\.[a-z\d\-]+)*+@[a-z\d\-]+(\.[a-z\d\-]+)*\.[a-z]+\z/i
    if email&.match?(valid_email_regex)
      link_to(email, "mailto:#{email}?Subject=Message")
    else
      email
    end
  end

  def table_row(area_display: true)
    event = @object
    markup(:tr) do |m|
      if area_display
        m.td class: 'center' do
          m << JpPrefecture::Prefecture.find(area)&.area
        end
      end
      m.td class: 'center' do
        m << decorated_dates(start_date, end_date, schedule_adjustment)
        if schedule_adjustment.present?
          # puts "#{schedule_adjustment} desu"
        end
      end
      m.td class: 'center'  do
        m << markdown(name)
        # m << '<br>'
        m << place
        if (attached_file = event&.s3files[0]).file_name.present?
          m << '<br>'
          m << link_to(attached_file.title, view_context.viewer_path(attached_file.id))
        end
      end
      m.td class: 'center'  do
        m << decorated_contact(contact, url)
        m << '<br>'
        m << tel_to(phone)
        m << '<br>'
        m << mail_to(email)
      end
    end
  end

  def delete_button(text = '削除')
    event = @object
    link_to text, view_context.event_path(event, view_context.controller.controller_name.singularize), method: :delete, data: { confirm: '本当に削除しますか' }, class: "btn btn-danger btn-sm"
  end

  def delete_attached_file_button(text = "添付<br>削除")
    event = @object
    if event&.s3files[0]&.file_name.present?
      link_to text.html_safe, view_context.s3file_path(event.s3files[0], view_context.controller.controller_name.singularize), method: :delete, data: { confirm: '本当に削除しますか' }, class: "btn btn-warning btn-sm"
    end
  end


  def finish_edit_event_link_button(text)
    link_to text, view_context.root_path, class: 'btn btn-primary btn-sm'
  end

  def edit_user_link_button
    user = @object
    link_to "情報編輯", [:edit, user], class: 'btn btn-warning btn-sm'
  end

  def edit_user_profile_link_button
    user = @object
    link_to "紹介編輯", [:edit, user, :profile], class: 'btn btn-warning btn-sm'
  end

  def approbation_link_button
    user = @object
    link_to "入会承認", view_context.approbation_path(user), class: 'btn btn-success btn-sm', data: { toggle: 'modal', target: "#approbation_#{user.id}" }
  end

  def rejection_link_button
    user = @object
    link_to "入会却下", view_context.rejection_path(user), class: 'btn btn-danger btn-sm', data: { toggle: 'modal', target: "#rejection_#{user.id}" }
  end

  def approbation_modal
    user = @object
    markup(:div, class: 'modal fade', id: "approbation_#{user.id}", role: 'dialog', 'area-hidden' => true) do |m|
      m.div(class: 'modal-dialog', role: 'document') do
        m.div(class: 'modal-content') do
          m.form(action: view_context.approbation_path(user), 'accept-charset' => 'UTF-8', method: :get) do
            m.div(class: 'modal-header') do
              m.h4(class: 'modal-title text-success') { m.text "#{user.name}さん 入会承認" }
              m.button(type: 'button', class: 'close', 'data-dismiss' => 'modal', 'area-label' => 'close') do
                m.span('area-hidden' => true) { m.text "×" }
              end
            end

            m.div(class: 'modal-body') do
              m << '<label>件名</label>'
              approbation = WelcomeMessage.approbation
              subject = approbation.subject || "五級位上昇を目指す会 入会承認のご連絡"
              m << "<input type='text' name='subject' class='form-control' value='#{subject}'>"
              m << '<label>本文</label>'
              msg = <<~EOS
                ようこそ5JKへ！
                入会手続きが完了しましたので、
                ハンドルネームが<a href='https://shogi-5jk.herokuapp.com/users'>名簿</a>にあることをご確認下さい。
                一緒に卒業目指してがんばりましょう！

                五級位上昇を目指す会 会長 z-air-6
              EOS
              message = approbation.message || msg
              m << "<textarea name='message' class='form-control' >#{message}</textarea>"
            end

            m.div(class: 'modal-footer') do
              m.button(type: 'button', class: 'btn btn-light', 'data-dismiss' => 'modal') { m.text "キャンセル" }
              m.input(type: 'submit', value: '承認する', class: "btn btn-success", data: { disable_with: '送信中...' })
            end
          end
        end
      end
    end
  end

  def rejection_modal
    user = @object
    markup(:div, class: 'modal fade', id: "rejection_#{user.id}", role: 'dialog', 'area-hidden' => true) do |m|
      m.div(class: 'modal-dialog', role: 'document') do
        m.div(class: 'modal-content') do
          m.form(action: view_context.rejection_path(user), 'accept-charset' => 'UTF-8', method: :get) do
            m.div(class: 'modal-header') do
              m.h4(class: 'modal-title text-danger') { m.text "#{user.name}さん 入会却下" }
              m.button(type: 'button', class: 'close', 'data-dismiss' => 'modal', 'area-label' => 'close') do
                m.span('area-hidden' => true) { m.text "×" }
              end
            end

            m.div(class: 'modal-body') do
              m << '<label>件名</label>'
              rejection = WelcomeMessage.rejection
              subject = rejection.subject || "五級位上昇を目指す会 入会却下のご連絡"
              m << "<input type='text' name='subject' class='form-control' value='#{subject}'>"
              m << '<label>本文</label>'
              msg = <<~EOS
                5JKへの入会申し込みありがとうございます。
                せっかくお申し込み頂きましたが、
                規程の対局数（100局）に達していないため、
                また改めてお申し込み頂けますでしょうか？
                一緒に集える日を楽しみに致しております。

                五級位上昇を目指す会 会長 z-air-6
              EOS
              message = rejection.message || msg
              m << "<textarea name='message' class='form-control' >#{message}</textarea>"
              m.div(class: 'text-right') do
                m.small << '(※却下とともに入会希望者からの申請データも削除します)'
              end
            end

            m.div(class: 'modal-footer') do
              m.button(type: 'button', class: 'btn btn-light', 'data-dismiss' => 'modal') { m.text "キャンセル" }
              m.input(type: 'submit', value: '却下する', class: "btn btn-danger", 'data-confirm' => '本当に却下しますか', data: { disable_with: '送信中...' })
            end
          end
        end
      end
    end
  end
end
