class FormPresenter
  include HtmlBuilder
  include Avatar
  include SessionsHelper

  attr_reader :form_builder, :view_context
  delegate :label, :text_field, :email_field, :password_field, :hidden_field, :check_box,
           :radio_button, :text_area, :object, to: :form_builder
  delegate :raw, :link_to, :image_tag, to: :view_context

  def initialize(form_builder, view_context)
    @form_builder = form_builder
    @view_context = view_context
  end

  def date_field_block(name, placeholder_text)
    markup(:div, class: 'form-group') do |m|
      m << text_field(name, data: { provide: 'datepicker' }, placeholder: placeholder_text, class: 'text-center', skip_label: true)
    end
  end

  def schedule_adjustment_block
    markup(:div, class: 'form-group') do |m|
      m << text_field(:schedule_adjustment, placeholder: '調整中など', class: 'text-center', skip_label: true)
    end
  end

  def text_field_block(name, placeholder_text)
    markup(:div, class: 'form-group') do |m|
      m << text_field(name, placeholder: placeholder_text, class: 'text-left', skip_label: true)
    end
  end

  def hidden_field_block(name, value)
    markup(:div, class: 'form-group') do |m|
      m.input(type: 'hidden', name: "event[#{name}]", value: value)
    end
  end

  def display_check_box(id: nil)
    f = @form_builder
    markup do |m|
      m << f.check_box(:display, id: "check_box_#{id}", label: '表示')
      # m << f.label('表示する', for: "check_box_#{id}")
   end
  end

  def update_button(text: '更新', size: :sm, color: :success)
    markup do |m|
      m.input(type: 'submit', value: text, class: "btn btn-#{size} btn-#{color}")
    end
  end

  def append_button(text: '追加', size: :sm, color: :primary)
    markup do |m|
      m.input(type: 'submit', value: text, class: "btn btn-#{size} btn-#{color}")
    end
  end

  def upload_file_block(uploaded_file_name: nil, accept: :pdf)
    markup(:div, class: 'input-group mb-3') do |m|
      m.div(class: 'custom-file') do
        format = case accept.to_sym
                 when :pdf
                   'application/pdf'
                 when :image
                   'image/jpeg, image/gif, image/png'
                 when :excel
                   'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'
                 end
        m.input(type: 'file', name: 'upload_file', class: 'custom-file-input', accept: format)
        m.label(class: "custom-file-label custom-file-label-#{accept} text-left", title: uploaded_file_name) do
          m.text uploaded_file_name
        end
      end
    end
  end

  def link_field_block
    link = @form_builder.object
    markup(:div, class: 'form-group') do |m|
      m.div(class: 'col-12 form-inline') do
        m.input(type: 'text', name: 'link[text]', class: 'form-control col-4', value: link.text)
        m.div(class: 'col-6 pl-3') do
          m.input(type: 'text', name: 'link[url]', class: 'form-control', value: link.url, style: 'width: 100%')
        end
        m.div(class: 'col-1') do
          m << delete_link_button(link)
        end
        m.div(class: 'col-1') do
          m << update_button
        end
      end
    end
  end

  def append_link_field_block
    link = @form_builder.object
    markup(:div, class: 'form-group') do |m|
      m.div(class: 'col-12 form-inline') do
        m.input(type: 'text', name: 'link[text]', placeholder: '説明文', class: 'form-control col-4')
        m.div(class: 'col-6 pl-3') do
          m.input(type: 'text', name: 'link[url]', placeholder: 'URL', class: 'form-control', style: 'width: 100%')
        end
        m.div(class: 'col-2') do
          m << append_button(text: 'リンクを追加する')
        end
        # m << hidden_field(:section, value: type)
      end
    end
  end

  def delete_link_button(link)
    view_context.link_to("削除", view_context.external_link_path(link), method: :delete, data: { confirm: '本当に削除しますか' }, class: "btn btn-danger btn-sm")
  end

  AREAS = [[:関東, 8],
           [:中部, 15],
           [:関西, 24],
           [:中四国, 31],
           [:九州, 40],
           [:沖縄, 47],
           [:本戦, 48],
           [:シード選手, 49]]
  def drop_down_list_block(name, label_text, choices = AREAS, options = { class: 'searchable' })
    markup do |m|
      m << form_builder.select(name, choices, { skip_label: true }, options)
    end
  end
end
