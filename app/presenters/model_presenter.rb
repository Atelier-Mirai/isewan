# https://www.oiax.jp/rails/tips/decorators_and_presenters.html
class ModelPresenter
  include HtmlBuilder
  include ApplicationHelper
  include MarkdownHelper

  attr_reader :object, :view_context
  delegate :created_at, :updated_at, to: :object
  delegate :raw, :link_to, :image_tag, :sanitize, :label, to: :view_context

  def initialize(object, view_context)
    @object       = object
    @view_context = view_context
  end

  # build メソッドの定義
  class << self
    def build(objects, view_context)
      objects&.each { |object| yield new(object, view_context) }
    end
  end

  def edit_link_button(text = nil)
    link_to text || "編輯", [:edit, @object], class: 'btn btn-warning btn-sm'
  end

  def delete_link_button(text = nil)
    link_to text || "削除", @object, 'data-confirm' => '本当に削除しますか', rel: 'nofollow', 'data-method' => 'delete', class: 'btn btn-danger btn-sm'
  end
end
