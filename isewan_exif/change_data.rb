# 写真の撮影日時を変更する
# https://qiita.com/okitan/items/9a0178b10b9023249f08

require 'mini_exiftool'
require 'pathname'
require 'time'

# Dir.glob("*.JPG") do |f|
#   File.rename(f, f.downcase)
# end
#

# Dir.glob("*.jpg") do |f|
#   puts f
# end
#
photos = Dir.glob("*.jpg").sort.map { |f| File.split(f)[1] }.sort

times = {}
times['a'] = Time.new(2019, 8, 3, 9, 0, 0)
times['b'] = Time.new(2019, 8, 4, 9, 0, 0)

photos.each do |photo_name|
# Pathname.glob('*.jpg') do |f|
  type = photo_name[0]
  number = photo_name[1..-1].to_i
  puts type, number

  Dir.glob(photo_name) do |f|
    mtime = times[type] + number * 60
    File.utime(mtime, mtime, f)
  end

  photo = MiniExiftool.new(photo_name, ignore_minor_errors: true)
  photo.date_time_original = times[type] + number * 60

  begin
    photo.save!
    puts "update succeeded for #{photo_name}"
  rescue => e
    puts e
  end
end
