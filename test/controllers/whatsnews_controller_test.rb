require 'test_helper'

class WhatsnewsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @whatsnews = whatsnews(:one)
  end

  test "should get index" do
    get whatsnews_url
    assert_response :success
  end

  test "should get new" do
    get new_whatsnews_url
    assert_response :success
  end

  test "should create whatsnews" do
    assert_difference('Whatsnew.count') do
      post whatsnews_url, params: { whatsnews: {  } }
    end

    assert_redirected_to whatsnews_url(Whatsnew.last)
  end

  test "should show whatsnews" do
    get whatsnews_url(@whatsnews)
    assert_response :success
  end

  test "should get edit" do
    get edit_whatsnews_url(@whatsnews)
    assert_response :success
  end

  test "should update whatsnews" do
    patch whatsnews_url(@whatsnews), params: { whatsnews: {  } }
    assert_redirected_to whatsnews_url(@whatsnews)
  end

  test "should destroy whatsnews" do
    assert_difference('Whatsnew.count', -1) do
      delete whatsnews_url(@whatsnews)
    end

    assert_redirected_to whatsnews_url
  end
end
